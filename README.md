# 2012 President Twitter Sentiment Analysis

### Fangda Fan, Xiaohan Liu

### April 2016

## Data

Tweets related with 2012 US president candidates Obama and Romney (each about 7200 tweets) during October 12-16, 2012

Sentiments labelled with -1 (negative), 0 (neutral), 1 (positive) and 2 (mixed).

Need to classify tweets with -1, 0, and 1 (2 is omitted).

## Requirements

- Platform: [Anaconda 4.3](https://www.continuum.io/downloads) (Python 3.6)

- Package for neural network: [Keras](https://github.com/fchollet/keras) with [Tensorflow](https://www.tensorflow.org/) or Theano backend
```sh
pip install keras
```
- Package for dataframe-based machine learning: [DFlearn](https://github.com/founderfan/DFlearn)
```sh
pip install dflearn==0.1.9
```
- Packages for advanced gradient boosting models: [LightGBM](https://github.com/Microsoft/LightGBM) and [XGBoost](https://github.com/dmlc/xgboost)
- NLTK package data download in python
- [GloVe](http://nlp.stanford.edu/projects/glove/) 27B twitter dictionary (already included in ./saved\_data)

## Data Mining Methods

### 1. Bag-of-Word Analysis

Includes the following features:
1. TF-IDF vectorization of sentences
2. [Liu](https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html#lexicon)'s opinion lexicon counting
3. [Vader](http://www.nltk.org/_modules/nltk/sentiment/vader.html) sentiment analyzer
4. Date-time variables

Models:

- Bernoulli Naive Bayes
- Random Forest
- AdaBoost
- Gradient Boosting Tree
- XGBoost
- LightGBM
- SVM

### 2. Sequential Word Analysis

Includes the following features:

1. Word vectorization from [GloVe](http://nlp.stanford.edu/projects/glove/) 27B twitter dictionary
2. [Part-of-speech tagging](http://www.nltk.org/book/ch05.html)
3. [SentiWordNet](http://sentiwordnet.isti.cnr.it/)

Models:

- Deep Neural Network: CNN+LSTM